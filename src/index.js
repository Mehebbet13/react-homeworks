import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {BrowserRouter} from 'react-router-dom'
import * as serviceWorker from './serviceWorker';
// import TheMainPage from "./Routing/TheMainPage";
 import App from "./Homework3/App";
 //import App from "./Homework2/App";//if You want to see hw2 uncomment this line and comment the line in the above


ReactDOM.render(
    <BrowserRouter>
    <App/>
    </BrowserRouter>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
