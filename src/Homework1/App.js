import React, {Component} from 'react';
import Button from "./Button";
import Modal from "./Modal";
import './style.css'



class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            headerText: '',
            isCrossIcon: false,
            mainText: '',
            buttons: [],
            openFirstModalWindow: false,
            openSecondModalWindow: false
        };
    }

    closeFirstModalWindow = (e) => {

        if (e.currentTarget.classList.contains('background')){
            if (e.target === e.currentTarget){
                console.log(e.target,e.currentTarget);
                this.setState(function (prevState) {
                    return {openFirstModalWindow: !prevState.openFirstModalWindow}
                });
            }
        }else{
            this.setState(function (prevState) {
                return {openFirstModalWindow: !prevState.openFirstModalWindow}
            });
        }
    };

    closeSecondModalWindow = (e) => {
        if (e.currentTarget.classList.contains('background')){
            if (e.target === e.currentTarget){
                this.setState(function (prevState) {
                    return {openSecondModalWindow: !prevState.openSecondModalWindow}
                });
            }
        }else{
            this.setState(function (prevState) {
                return {openSecondModalWindow: !prevState.openSecondModalWindow}
            });
        }
    };

    handleFirstButtonClick = () => {

        this.setState(function (prevState) {
            return {openFirstModalWindow: !prevState.openFirstModalWindow}
        });
        this.showFirstModalWindow();
    };

    handleSecondButtonClick = () => {
        this.setState(function (prevState) {
            return {openSecondModalWindow: !prevState.openSecondModalWindow}
        });
        this.showSecondModalWindow();
    };

    showFirstModalWindow = () => {
        if (this.state.openFirstModalWindow) {

            const button1 = <Button
                bgColor='#b3382c'
                text='Ok'
                onClick={this.closeFirstModalWindow}
            />;

            const button2 = <Button
                bgColor='#b3382c'
                text='Cancel'
                onClick={this.closeFirstModalWindow}
            />;

            return (
                <Modal
                    headerText='Do you want to delete this file?'
                    isCrossIcon={true}
                    mainText='Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'
                    buttons={[button1, button2]}
                    closeModalWindow={this.closeFirstModalWindow}
                />
            );
        }
    };

    showSecondModalWindow = () => {
        if (this.state.openSecondModalWindow) {

            const button1 = <Button
                bgColor='#b3382c'
                text='Ok'
                onClick={this.closeSecondModalWindow}
            />;

            const button2 = <Button
                bgColor='#b3382c'
                text='Cancel'
                onClick={this.closeSecondModalWindow}
            />;

            return (
                <Modal
                    headerText='Do you want to save this file?'
                    isCrossIcon={true}
                    mainText='It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. '
                    buttons={[button1, button2]}
                    closeModalWindow={this.closeSecondModalWindow}

                />
            );
        }
    };

    render() {
        return (
            <div>
                <Button
                    bgColor='red'
                    text='Delete'
                    onClick={this.handleFirstButtonClick}
                />
                <Button
                    bgColor='green'
                    text='Save'
                    onClick={this.handleSecondButtonClick}
                />

                {this.showFirstModalWindow()}
                {this.showSecondModalWindow()}

            </div>
        );
    }
}

export default App;