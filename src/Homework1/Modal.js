import React, {Component} from 'react';
import './style.css';
import cross from './cross.png'

class Modal extends Component {

    displayCrossButton = () => {
        if (this.props.isCrossIcon) {
            return <button className={'modal-close-button'} onClick={this.props.closeModalWindow} >
                <img src={cross} alt="cross"/>
            </button>
        }
    };

    render() {
        return (
            <div className={'background'} onClick={this.props.closeModalWindow}>
                <div className="modal-window">
                    <div className={'modal-header'}>{this.props.headerText} {this.displayCrossButton()}</div>
                    <div className="modal-container">
                        <div className="modal-text-container"><p className={'main-text'}>{this.props.mainText}</p></div>
                        <div className="modal-buttons-container">{[...this.props.buttons]}</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;