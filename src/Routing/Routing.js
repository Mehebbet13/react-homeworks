import React, {Component} from 'react';
import {BrowserRouter as Router,
Switch,
Route,
Link} from "react-router-dom"
import About from "./About";
import Home from "./Home";
import Users1 from "./Users1";
class Routing extends Component {
    render() {
        return (
            <Router>
                <div>
                    <h2>React route is a separate library!</h2>
                    <p>We need to download it every time for every project</p>
                    <p>download command - npm install react-router react-router-dom --save</p>
                        <ul>
                            <li>
                                <Link to={"/"}>Home</Link>
                            </li>
                            <li>
                                <Link to="/about">About</Link>
                            </li>
                            <li>
                                <Link to="/users">Users</Link>
                            </li>
                        </ul>
                    <Switch>
                        <Route path={"/about"}>
                                <About/>
                            </Route>
                            <Route path={"/users"}>
                                <Users1/>
                            </Route>
                            <Route path={"/"}>
                                <Home/>
                            </Route>
                            </Switch>
                            </div>
            </Router>
        );
    }
}

export default Routing;