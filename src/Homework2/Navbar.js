import React, {Component} from 'react';

class Navbar extends Component {
    render() {
        return (
            <div className={"navbar"}>
                <div className={"logo"}>
                    <div>
                        <img src={require("./images/logo-icon.png")} alt=""/>

                    <span>Store</span>
                    </div>
                </div>

                <div className={"nav"}>
                    <div>
                        <a href="">HOME</a>
                        <a href="">CD's</a>
                        <a href="">LOREM</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Navbar;