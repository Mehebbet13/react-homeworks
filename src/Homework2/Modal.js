import React, {Component} from 'react';

class Modal extends Component {

    render() {
        return (

            <div className={"modal-text"}>
               <p>You added this item to Shopping Cart.Do You want to see those items?</p>
                <button className={"btn"} onClick={this.props.hideCart}>OK</button>
                <button className={"btn"} onClick={this.props.hideCart}>Cancel</button>
            </div>
        );
    }
}

export default Modal;