import React, {Component} from 'react';
import "./style.scss";

class TopOfPage extends Component {
    render() {
        return (
            <div className={"top-of-page"}>
                <div className={"social-icons"}>
                <div className={"icon"}> <img src={require("./images/facebook.png")} alt=""/> </div>
                <div className={"icon"}><img src={require("./images/mail.png")} alt=""/></div>
                <div className={"icon"}><img src={require("./images/dribble.png")} alt=""/></div>
                <div className={"icon"}><img src={require("./images/twitter.png")} alt=""/></div>
                <div className={"icon"}><img src={require("./images/vimeo.png")} alt=""/></div>
                </div>

                <div className={"login-links"}>
                    <a href="#"> Login</a>
                    <span>/</span>
                    <a href="#"> Register</a>
                    <div className={"cart"} onClick={this.props.hideCart}><img src={require("./images/cart.png")} alt=""/> <span>Cart()</span></div>

                </div>
            </div>
        );
    }
}

export default TopOfPage;