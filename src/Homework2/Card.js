import React, {Component} from 'react';
import "./style.scss"
class Card extends Component {
    addToCart=(e)=>{

    };
    render() {
        // console.log(this.props.product.id)
        return (
<>
            <div className={"card"} id={this.props.product.id}>
                <img src={this.props.product.imgUrl} alt=""/>
                <div className={"text-container"}>
              <span><strong>{this.props.product.name}</strong> <i> by Artist</i></span>
                    <span onClick={this.props.handleFav} className="fa fa-star star-icon "> </span>
                    <p className={"text"}>Lorem ipsum dolor sit amet, con
                        adipiscing elit, sed diam nonu. </p>
                <strong className={"price"}>${this.props.product.price}</strong>
                    <div onClick={this.props.openCart }  className={"button"}>ADD TO CART</div>
                </div>
            </div>
    </>

        );
    }
}

export default Card;