import React, {Component} from 'react';
import Card from "./Card";
import Modal from "./Modal";
import TopOfPage from "./TopOfPage";



class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            showModal: false,
            selectedCards: [],
            favCards: []

        };
    }

    // setItemLocalStorage=(e)=>{
    //
    // };
    let;
    showCart = (e) => {

        // console.log(e.target.parentElement.parentElement.getAttribute('id'));
        //    const modalCards =  this.state.products.filter((card)=>parseInt(e.target.parentElement.parentElement.getAttribute('id')) === card.id);
        //    console.log(modalCards);
        //    return <Modal product={modalCards} />

        const selectedCard = this.state.products.filter((card) => parseInt(e.target.parentElement.parentElement.getAttribute('id')) === card.id);
        console.log(selectedCard);
        // let selectedCards= this.state.selectedCards.push(selectedCard);
        this.state.selectedCards.push(selectedCard);
        this.setState({showModal: true, selectedCards: this.state.selectedCards});
        console.log(this.state.selectedCards);
        localStorage.setItem("Cards", JSON.stringify(this.state.selectedCards));
    };

    addFavorites = (e) => {
        const favCard = this.state.products.filter((card) => parseInt(e.target.parentElement.parentElement.getAttribute('id')) === card.id);
        if (!e.target.classList.contains("checked")) {
            e.target.className += " checked";
            this.state.favCards.push(favCard);
            this.setState({ favCards: this.state.selectedCards});
            console.log(this.state.favCards);
            localStorage.setItem("Favorites", JSON.stringify(this.state.favCards));
        } else {
            e.target.classList.remove("checked");
        }
        console.log(favCard.className);
        console.log(e.currentTarget);
    };
    hideCart = () => {
        this.setState({showModal: false});
    };

    hideCartOutside = (e) => {
        if (this.state.showModal) {
            this.setState({showModal: false});
        }
    };

    componentDidMount() {

        fetch("/data.json").then(r => r.json())
            .then(data => {
                this.setState({
                    products: data
                });
            });
    }

    render() {
        const cards = this.state.products.map((item) => {
            return <Card product={item} key={item.id} openCart={this.showCart} handleFav={this.addFavorites}/>;});


        return (
            <>
                <TopOfPage hideCart={this.hideCart}/>
                <div className={"container"} onClick={this.hideCartOutside}>

                    {

                        this.state.products.length > 0 ?
                            cards
                            : "loading"
                    }
                </div>
                {
                    this.state.showModal ?
                        <div className={"modal-cart"}>

                            {
                                <Modal hideCart={this.hideCart}/>
                            }
                        </div> : null
                }
            </>
        );
    }
}

export default App;