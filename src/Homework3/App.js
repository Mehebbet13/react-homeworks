import React, {useState, useEffect} from 'react';
import Card from "./Card";
import TheMainPage from "./TheMainPage";


const App = (props) => {

    const [products, setProducts] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [selectedCards, setSelectedCards] = useState([]);
    const [favCards, setFavCards] = useState([]);
    const [openConfirmModal, setOpenConfirmModal] = useState(false);

    const showCart = (e) => {
        const selectedCard = products.filter((card) => parseInt(e.target.parentElement.parentElement.getAttribute('id')) === card.id);
        selectedCards.push(selectedCard);
        setShowModal(true);
        setSelectedCards(selectedCards);
        localStorage.setItem("Cards", JSON.stringify(selectedCards));
    };
    const openModal = () => {
        setOpenConfirmModal(true);
    };

   const deleteCard=(e)=>{
       const id=parseInt(e.target.parentElement.parentElement.getAttribute('id')) ;
       const selectedCard = products.filter((card) => id === card.id);
       const ind=selectedCards.indexOf(selectedCard);
       selectedCards.splice(ind,1);
       e.target.parentElement.parentElement.parentElement.remove();
       setSelectedCards(selectedCards);
       setOpenConfirmModal(false);

   };
  const hideModal=()=>{
      setOpenConfirmModal(false);
  };
    const addFavorites = (e) => {
        const favCard = products.filter((card) => parseInt(e.target.parentElement.parentElement.getAttribute('id')) === card.id);
        if (!e.target.classList.contains("checked")) {
            e.target.className += " checked";
            favCards.push(favCard);
            setFavCards(favCards);
            localStorage.setItem("Favorites", JSON.stringify(favCards));
        } else {
            e.target.classList.remove("checked");
        }
    };
    const hideCart = () => {
        setShowModal(false);
    };

    const hideCartOutside = (e) => {
        if (showModal) {
            setShowModal(false);
        }
    };

    useEffect(() => {
        fetch("/data.json").then(r => r.json())
            .then(data => {
                setProducts(data);
            });
    },[products.length]);
    const cards = products.map((item) => {
        return <Card product={item} key={item.id} openCart={showCart}  handleFav={addFavorites}/>;
    });
    return (
        <>
            <TheMainPage cards={cards}
                         products={products}
                         hideCartOutside={hideCartOutside}
                         hideCart={hideCart}
                         showModal={showModal}
                         favCards={favCards}
                         selectedCards={selectedCards}
                         deleteCard={deleteCard}
                         openModal={openModal}
                         openConfirmModal={openConfirmModal}
                         hideModal={hideModal}
                         addFavorites={addFavorites}
            />

        </>
    );
};


export default App;