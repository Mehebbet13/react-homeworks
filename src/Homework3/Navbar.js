import React from 'react';
import {
    Link
} from "react-router-dom";

const Navbar =()=> {

        return (
            <>
                <div className={"navbar"}>

                        <div className={"logo"}>
                            <div>
                                <img src={require("./images/logo-icon.png")} alt=""/>

                                <span>Store</span>
                            </div>
                        </div>

                        <div className={"nav"}>
                            <Link to={'/'}>Home</Link>
                            <Link to={'/favorites'}>Favorites</Link>
                            <Link to={'/cartcards'}>Cart Cards</Link>

                        </div>

                </div>
                <br/> <br/> <br/>
            </>
        );
    };

export default Navbar;