import React from 'react';
import "./style.scss"

const Card = (props) => {

    return (
        <>
            <div className={"card"} id={props.product.id}>
                <img className={"img"} src={props.product.imgUrl} alt=""/>
                <div className={"text-container"}>
                    <span><strong>{props.product.name}</strong> <i> by Artist</i></span>
                    <span onClick={props.handleFav} className="fa fa-star star-icon "> </span>
                    <p className={"text"}>Lorem ipsum dolor sit amet, con
                        adipiscing elit, sed diam nonu.
                    </p>
                    <strong className={"price"}>${props.product.price}</strong>
                    <div className={"button"} onClick={props.openCart}>ADD TO CART</div>
                </div>
            </div>
        </>

    );
};

export default Card;