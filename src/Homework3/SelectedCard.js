import React from 'react';
import cross from "./images/cross.png"
import ModalConfirm from "./ModalConfirm";
const SelectedCard = (props) => {

    return (
        <>
            <div className={"card"} id={props.product.id}>
                <img className={"img"} src={props.product.imgUrl} alt=""/>
                <div className={"text-container"}>
                    <span><strong>{props.product.name}</strong> <i> by Artist</i></span>
                     <span onClick={props.addFavorites} className="fa fa-star star-icon "> </span>
                    <p className={"text"}>Lorem ipsum dolor sit amet, con
                        adipiscing elit, sed diam nonu. </p>
                    <strong className={"price"}>${props.product.price}</strong>
                    <button className={'modal-close-button'} onClick={props.openModal} >
                        <img className={"cross"} src={cross} alt=""/>
                    </button>
                </div>
            </div>
            {props.openConfirmModal?
            <ModalConfirm delete={props.deleteCard} hideModal={props.hideModal}/>
            :null
            }
        </>
    );
};

export default SelectedCard;