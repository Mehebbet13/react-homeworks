import React from 'react';
import Modal from "./Modal";

const AllCards = (props) => {
    return (
        <>
            <div className={"container"} onClick={props.hideCartOutside}>

                {

                    props.products.length > 0 ?
                        props.cards
                        : "loading"
                }
            </div>
            {
                props.showModal ?
                    <div className={"modal-cart"}>

                        {
                            <Modal hideCart={props.hideCart}/>
                        }
                    </div> : null
            }
        </>
    );
};

export default AllCards;