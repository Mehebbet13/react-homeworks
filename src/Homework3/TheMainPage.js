import React from 'react';
import {
    Route,
    Switch,
} from "react-router-dom";
import Favorites from "./Favorites";
import Navbar from "./Navbar";
import TopOfPage from "./TopOfPage";
import AllCards from "./AllCards";
import CartCards from "./CartCards";


const TheMainPage =(props)=> {

        return (
            <>

                <TopOfPage />
                <Navbar/>

                    <Switch>
                        <Route path={"/favorites"}>
                            <Favorites favCards={props.favCards} deleteCard={props.deleteCard}/>
                        </Route>
                        <Route path={"/cartcards"}>
                            <CartCards selectedCards={props.selectedCards}
                                       deleteCard={props.deleteCard}
                                       openModal={props.openModal}
                                       openConfirmModal={props.openConfirmModal}
                                       hideModal={props.hideModal}
                                       addFavorites={props.addFavorites}
                            />
                        </Route>
                        <Route exact  path={"/"}>
                            <AllCards cards={props.cards} products={props.products} hideCartOutside={props.hideCartOutside} hideCart={props.hideCart} showModal={props.showModal}/>
                        </Route>
                    </Switch>

                </>
        );

};

export default TheMainPage;