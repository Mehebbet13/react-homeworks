import React from 'react';
import './style.scss';
import cross from './images/cross.png'

const ModalConfirm = (props) => {


    return (
        <div className={'background'} >
            <div className="modal-window">
                <div className={'modal-header'}><img className={"modal-cross"} src={cross}  onClick={props.hideModal} alt=""/>  </div>
                    <div className="modal-container">
                        <div className="modal-text-container"><p className={'main-text'}> Do You Want To delete this item from Cart</p></div>
                        <div className="modal-buttons-container">
                            <button className={"btn"} onClick={props.delete}>OK</button>
                            <button className={"btn"}  onClick={props.hideModal} >Cancel</button>
                        </div>
                    </div>


            </div>
        </div>
    );

};

export default ModalConfirm;