import React from 'react';
import SelectedCard from "./SelectedCard";


const CartCards =(props)=>{
    const selectedCards = props.selectedCards.map((item) => {
        return <SelectedCard product={item[0]} key={item[0].id} deleteCard={props.deleteCard} openModal={props.openModal}
                             openConfirmModal={props.openConfirmModal}
                             hideModal={props.hideModal}
                             addFavorites={props.addFavorites}

        />;});
    return (
        <>
            <div className={"container"}>
                <div className={'header'}>
                        <h1>Cart</h1>
                </div>

                {

                    props.selectedCards.length > 0 ?
                        selectedCards
                        : "Please, add a card to Cart."
                }
            </div>
        </>
    );
};

export default CartCards;