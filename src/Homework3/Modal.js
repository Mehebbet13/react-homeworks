import React from 'react';
import {Link} from "react-router-dom";


const Modal =(props)=>{

        return (

                <div className={"modal-text"}>
                    <p>You added this item to Shopping Cart.Do You want to see those items?</p>
                    <Link to={'/cartcards'}><button className={"btn"} onClick={props.hideCart}>OK</button></Link>
                    <button className={"btn"} onClick={props.hideCart}>Cancel</button>
                </div>

        );
};

export default Modal;