import React from 'react';


import FavoriteCard from "./FavoriteCard";


const Favorites =(props)=>{
        const favCards = props.favCards.map((item) => {
            return <FavoriteCard product={item[0]} key={item[0].id} deleteCard={props.deleteCard} />;});
        return (
            <>
                <div className={"container"}>
                    <div className={'header'}>
                        <h1>Favorites</h1>
                    </div>

                    {

                        props.favCards.length > 0 ?
                            favCards
                            : "There is no Favorite Card."
                    }
                </div>
            </>
        );
};

export default Favorites;